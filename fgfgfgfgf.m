clear
% adds the jsonlab library to the path
addpath ('jsonlab');
% creates 3 headers
h1 = {'1. Katowice', '2. Bielsko Biała', '3. Raciborz', '4. Strona IMGW', '5. Strona WTTR'};
h2 = {'Station','City','Year','Month','Day','Hour','Temperature','Feels like Temperature', 'Pressure', 'Wind Speed', 'Relative Humidity '};
h3 = {' ',' ',' ',' ',' ','UTC', '°C', '°C', 'hPa','km/h', '%'};
if exist('weatherData.csv') == 2
    break
else
%open file and add headers

fid = fopen('weatherData.csv','w');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s\n', '"',h1{1},'";"',h1{2},'";"',h1{3},'";"',h1{4},'";"',h1{5},'"');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',h2{1},'";"',h2{2},'";"',h2{3},'";"',h2{4},'";"',h2{5},'";"',h2{6},'";"',h2{7},'";"',h2{8},'";"',h2{9},'";"',h2{10},'";"',h2{11},'"');
fprintf(fid,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '"',h3{1},'";"',h3{2},'";"',h3{3},'";"',h3{4},'";"',h3{5},'";"',h3{6},'";"',h3{7},'";"',h3{8},'";"',h3{9},'";"',h3{10},'";"',h3{11},'"');
fclose(fid);
% 
for each city, enter the data from the page in the table
for x = 1:1 % cell 1E12
%dane publiczne
%katowice
f1_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/katowice');
mat1_x=loadjson(f1_x);
%add numer of station
s1_x = 4;
%add numer of city
c1_x = 1;
t1_x = mat1_x.temperatura;
p1_x = mat1_x.cisnienie;
ws1_x = mat1_x.predkosc_wiatru;
h1_x = mat1_x.wilgotnosc_wzgledna;
ot1_x = mat1_x.godzina_pomiaru;
d1_x = sscanf(mat1_x.data_pomiaru, '%d');
%to add date separately, we must create matrix
%Feels like Temperature
flt1_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws1_x))) - (0.0124 * str2double(ws1_x))) * ((str2double(t1_x) - 33)));
%create matrix with data
part1_x = [s1_x, c1_x, d1_x(1,1), abs(d1_x(2,1)), abs(d1_x(3,1)), str2double(ot1_x), str2double(t1_x), flt1_x, str2double(p1_x), str2double(ws1_x), str2double(h1_x)];
%add matrix to file weatherData.csv
dlmwrite('weatherData.csv',part1_x, '-append','delimiter', ';');

%bielsko
f2_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/id/12600');
mat2_x=loadjson(f2_x);
s2_x = 4;
c2_x = 2;
t2_x = mat2_x.temperatura;
p2_x = mat2_x.cisnienie;
ws2_x = mat2_x.predkosc_wiatru;
h2_x = mat2_x.wilgotnosc_wzgledna;
ot2_x = mat2_x.godzina_pomiaru;
d2_x = sscanf(mat2_x.data_pomiaru, '%d');
%Feels like Temperature
flt2_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws2_x))) - (0.0124 * str2double(ws2_x))) * ((str2double(t2_x) - 33)));
part2_x = [s2_x, c2_x, d2_x(1,1), abs(d2_x(2,1)), abs(d2_x(3,1)), str2double(ot2_x), str2double(t2_x), flt2_x, str2double(p2_x), str2double(ws2_x), str2double(h2_x)];
%add matrix to file weatherData.csv
dlmwrite('weatherData.csv',part2_x, '-append','delimiter', ';');

%raciborz
f3_x = urlread('https://danepubliczne.imgw.pl/api/data/synop/format/json/station/raciborz');
mat3_x=loadjson(f3_x);
s3_x = 4;
c3_x = 3;
t3_x = mat3_x.temperatura;
p3_x = mat3_x.cisnienie;
ws3_x = mat3_x.predkosc_wiatru;
h3_x = mat3_x.wilgotnosc_wzgledna;
ot3_x = mat3_x.godzina_pomiaru;
d3_x = sscanf(mat3_x.data_pomiaru, '%d');
%Feels like Temperature
flt3_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws3_x))) - (0.0124 * str2double(ws3_x))) * ((str2double(t3_x) - 33)));
part3_x = [s3_x, c3_x, d3_x(1,1), abs(d3_x(2,1)), abs(d3_x(3,1)), str2double(ot3_x), str2double(t3_x), flt3_x, str2double(p3_x), str2double(ws3_x), str2double(h3_x)];
%add matrix to file weatherData.csv
dlmwrite('weatherData.csv',part3_x, '-append','delimiter', ';');

%wttr
%Katowice
f4_x = urlread('http://wttr.in/Katowice?format=j1');
dat4_x=loadjson(f4_x);
cel4_x = struct2cell(dat4_x);
mat4_x = cell2mat(cel4_x{1});
s4_x = 5;
c4_x = 1;
t4_x = mat4_x.temp_C;
p4_x = mat4_x.pressure;
ws4_x = mat4_x.windspeedKmph;
h4_x = mat4_x.humidity;
ti4_x = mat4_x.observation_time;
ot4_x = sscanf(ti4_x, '%d');
% hours is in 12h format as char, so to hours with PM we add 12h 
if (ti4_x(7) == 'A' || (ot4_x == 12) || (ot4_x == 00))
    ot4_x = ot4_x;
else 
    ot4_x = ot4_x+ 12;
end

date4_x{1,2} = dat4_x.weather{1,1}.date;
d4_x = sscanf(date4_x{1,2}, '%d');
%temperatura odczuwalna
flt4_x = mat4_x.FeelsLikeC;
% flt4_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws4_x))) - (0.0124 * str2double(ws4_x))) * ((str2double(t4_x) - 33)));
part4_x = [s4_x, c4_x, d4_x(1,1), abs(d4_x(2,1)), abs(d4_x(3,1)), datenum(ot4_x), str2double(t4_x), str2double(flt4_x), str2double(p4_x), str2double(ws4_x), str2double(h4_x)];
dlmwrite('weatherData.csv',part4_x, '-append','delimiter', ';');

% Bielsko
f5_x = urlread('http://wttr.in/bielsko_biala?format=j1');
dat5_x=loadjson(f5_x);
cel5_x = struct2cell(dat5_x);
mat5_x = cell2mat(cel5_x{1});
s5_x = 5;
c5_x = 2;
t5_x = mat5_x.temp_C;
p5_x = mat5_x.pressure;
ws5_x = mat5_x.windspeedKmph;
h5_x = mat5_x.humidity;
ti5_x = mat5_x.observation_time;
ot5_x = sscanf(ti5_x, '%d');
if (ti5_x(7) == 'A' || (ot5_x == 12) || (ot5_x == 00))
    ot5_x = ot5_x;
else 
    ot5_x = ot5_x+ 12;
end

date5_x{1,2} = dat5_x.weather{1,1}.date;
d5_x = sscanf(date5_x{1,2}, '%d');
%temperatura odczuwalna
flt5_x = mat5_x.FeelsLikeC;
% flt5_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws5_x))) - (0.0124 * str2double(ws5_x))) * ((str2double(t5_x) - 33)));
part5_x = [s5_x, c5_x, d5_x(1,1), abs(d5_x(2,1)), abs(d5_x(3,1)), ot5_x, str2double(t5_x), str2double(flt5_x), str2double(p5_x), str2double(ws5_x), str2double(h5_x)];
dlmwrite('weatherData.csv',part5_x, '-append','delimiter', ';');
% Raciborz
f6_x = urlread('http://wttr.in/raciborz?format=j1');
dat6_x=loadjson(f6_x);
cel6_x = struct2cell(dat6_x);
mat6_x = cell2mat(cel6_x{1});
s6_x = 5;
c6_x = 3;
t6_x = mat6_x.temp_C;
p6_x = mat6_x.pressure;
ws6_x = mat6_x.windspeedKmph;
h6_x = mat6_x.humidity;
ti6_x = mat6_x.observation_time;
ot6_x = sscanf(ti6_x, '%d');
if (ti6_x(7) == 'A' || (ot6_x == 12) || (ot6_x == 00))
    ot6_x = ot6_x;
else 
    ot6_x = ot6_x+ 12;
end
date6_x{1,2} = dat6_x.weather{1,1}.date;
d6_x = sscanf(date6_x{1,2}, '%d');
flt6_x = mat6_x.FeelsLikeC;
% flt6_x = ( 33 + (0.478 + (0.237 * sqrt(str2double(ws6_x))) - (0.0124 * str2double(ws6_x))) * ((str2double(t6_x) - 33)));
part6_x = [s6_x, c6_x, d6_x(1,1), abs(d6_x(2,1)), abs(d6_x(3,1)), ot6_x, str2double(t6_x), str2double(flt6_x), str2double(p6_x), str2double(ws6_x), str2double(h6_x)];
dlmwrite('weatherData.csv',part6_x, '-append','delimiter', ';');
% the tether updates every hour
pause(3600)
end

%create matrix with all data
M(1,1) = {'1. Katowice'};
M(1,2) = {'2. Bielsko Biala'};
M(1,3) = {'3. Raciborz'};
M(1,4) = {'4. IMGW Web'};
M(1,5) = {'5. WTTR Web'};
M(2,1) = {'Station'};
M(2,2) = {'City'};
M(2,3) = {'Year'};
M(2,4) = {'Month'};
M(2,5) = {'Day'};
M(2,6) = {'Hour'};
M(2,7) = {'Temperature'};
M(2,8) = {'Feels Like Temperature'};
M(2,9) = {'Pressure'};
M(2,10) = {'Wind Speed'};
M(2,11) = {'Relative Humidity'};
M(3,1) = {''};
M(3,2) = {''};
M(3,3) = {''};
M(3,4) = {''};
M(3,5) = {''};
M(3,6) = {'UTC'};
M(3,7) = {'Celsius Degrees'};
M(3,8) = {'Celsius Degrees'};
M(3,9) = {'hPa'};
M(3,10) = {'km/h'};
M(3,11) = {'%'};
